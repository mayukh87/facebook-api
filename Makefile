COMPOSER=composer --no-interaction --no-progress --optimize-autoloader

TMP=/tmp/showcase-build

phpcs: vendor
	./vendor/bin/phpcs --extensions=php --standard=dev/standard/Awesome -s src/

phpmd: vendor
	./vendor/bin/phpmd src/ text dev/standard/phpmd.xml

codestyle: phpcs phpmd jscs

jshint: node_modules
	./node_modules/.bin/jshint js/

jscs: node_modules
	./node_modules/.bin/jscs js/

migrate: vendor
	./vendor/bin/doctrine migrations:migrate --no-interaction

.PHONY: clean prepare package infra clean-infra phpcs phpmd codestyle test migrate jshint jscs vagrant-install
