<?php

namespace Controller;

use Facebook\Facebook;

class Controller {
    
    /**
     * @var \Silex\Application 
     */
    protected $app;
    protected $fb;

    public function __construct($app) 
    {
        $this->app = $app;
        $this->fb = $this->getFBAccess();
    }
    
    public function getFBAccess()
    {
        $fbCreds = array(
            'app_id' => '1238301302862614',
            'app_secret' => '23d3005e68e97ac1487810ac95f1aaac',
            'default_graph_version' => 'v2.4',
            'default_access_token' => '1238301302862614|23d3005e68e97ac1487810ac95f1aaac'
        );
        
        return new Facebook($fbCreds);
    }
}

