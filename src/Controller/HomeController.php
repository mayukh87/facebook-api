<?php

namespace Controller;

use Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Facebook\Helpers\FacebookRedirectLoginHelper;

class HomeController extends Controller {

    public function index() 
    {
        return $this->app['twig']->render('home.twig', array('error'=>'Error'));
    }

    public function login() {
        $session = $this->app['session'];
        $session->set('loggedInValue', true);
        
        $token = $this->app['security.token_storage']->getToken();
        echo $token;exit;
        
        $helper = $this->fb->getRedirectLoginHelper();
        $loginURL = $helper->getLoginUrl('http://facebook.dev/login-callback');

        return $this->app['twig']->render('admin/choose-login.twig', array('loginurl' => $loginURL));
    }

    public function loginCallback() {

        $helper = $this->fb->getRedirectLoginHelper();
        $session = $this->app['session'];
        $session->get('sessionValue');
        try {
            $accessToken = $helper->getAccessToken();
        } catch (\Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
        }

        if (isset($accessToken)) {
            // Logged in!
            $this->app['session']->set('facebook_access_token', $accessToken);

            // Now you can redirect to another page and use the
            // access token from $_SESSION['facebook_access_token']
        }
    }

    public function url() 
    {
        if (isset($_SERVER['HTTPS'])) {
            $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
        } else {
            $protocol = 'http';
        }
        return $protocol . "://" . $_SERVER['HTTP_HOST'];
    }

}
