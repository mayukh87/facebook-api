<?php

use Controller\HomeController;

$app['home.controller'] = $app->share(function() use ($app) {
    return new HomeController($app);
});

$baseUrl = "/";

$app->get("$baseUrl", 'home.controller:index')->bind('homepage');
$app->post("$baseUrl/login", 'home.controller:login');
$app->get("$baseUrl/login-callback", 'home.controller:loginCallback');