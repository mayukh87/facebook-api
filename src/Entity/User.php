<?php

namespace Entity;

/**
 * User
 *
 * @Table(name="users")
 * @Entity
 */
class User {

    /**
     * @var integer
     *
     * @Column(type="integer")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(type="string", length=50)
     */
    private $username;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false, unique=true)
     */
    private $email;
    
    /**
     * @var string
     *
     * @Column(type="string", length=255)
     */
    private $password;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $username
     * @return User
     */
    public function setUserName($username) {
        $this->username = $username;
        
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getUserName() {
        return $this->username;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email) {
        $this->email = $email;
        
        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }
    
    public function getPassword() {
        return $this->password;
    }

    public function setPassword($password) {
        $this->password = $password;
        return $this;
    }

}
