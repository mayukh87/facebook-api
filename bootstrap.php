<?php

require './vendor/autoload.php';

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

// Create a simple "default" Doctrine ORM configuration for Annotations
$isDevMode = true;
$config = Setup::createAnnotationMetadataConfiguration(array(__DIR__ . "/src"), $isDevMode);

// Obtaining the entity manager
$entityManager = EntityManager::create(array(
                'driver' => 'pdo_mysql',
                'host' => 'localhost',
                'user' => 'root',
                'password' => '123',
                'dbname' => 'facebook',
                'charset' => 'utf8'
            ),
        $config
    );
/* @var $entityManager EntityManager */


