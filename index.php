<?php

require_once './bootstrap.php';

use Controller\Application;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\SessionServiceProvider;
use Silex\Provider\UrlGeneratorServiceProvider;
use Silex\Provider\SecurityServiceProvider;

$app = new Application();
$app->register(new ServiceControllerServiceProvider());
$app->register(new UrlGeneratorServiceProvider());
$app->register(new TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '/views',
));
$app->register(new SecurityServiceProvider(), array(
    'security.firewalls' => array()
));

/* @var $entityManager \Doctrine\ORM\EntityManager */

$app['doctrine'] = $entityManager;

$app->register(new SessionServiceProvider());
$app['session.storage.handler'] = null;

require_once './routes.php';
//$app->boot();
$app->run();
